# fix ubuntu azalia
just a quick fix to make playing sound via mainboard sound chip working again

+ Ubuntu 20.10 (Groovy Gorilla)
+ Audio device: Advanced Micro Devices, Inc. [AMD/ATI] SBx00 Azalia (Intel HDA) (rev 40) 

found here: [Thread: AMD Azalia audio works on live CD but fails after installation (18.04)](https://ubuntuforums.org/showthread.php?t=2398891)
